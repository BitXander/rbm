import time
import pytest
from selenium.webdriver.chrome.webdriver import WebDriver


@pytest.fixture
def driver(request):
    print('start fixture')
    chrome_driver = WebDriver(executable_path='C://selenium//chromedriver.exe')
    chrome_driver.implicitly_wait(3)
    yield chrome_driver
    print('stop fixture')
    chrome_driver.close()

# ввести корректный логин, ввести корректный пароль
def test_fixture_pytest8(driver: WebDriver):
    print('start test method invocation8')
    driver.get('https://rbm-test.brdo.com.ua/login')
    driver.maximize_window()
    time.sleep(1)
    driver.find_element_by_id("loginform-username").send_keys("ndividualkim17+9@gmail.com")
    time.sleep(1)
    driver.find_element_by_id("loginform-password").send_keys("123456")
    driver.find_element_by_name("login-button").click()
    time.sleep(1)
    driver.find_element_by_class_name("btn-carret").click()
    time.sleep(1)
    icon_profile = driver.find_element_by_xpath("/html/body/header/div/div/div/div/div[2]/nav/ul[2]/li[2]/div[2]/ul/li[1]")
    assert icon_profile.text == "ndividualkim17+9@gmail.com"
    print('stop test method invocation8')

# отправка пустой формы
def test_fixture_pytest1(driver: WebDriver):
    print('start test method invocation1')
    driver.get('https://rbm-test.brdo.com.ua/login')
    driver.find_element_by_name("login-button").click()
    time.sleep(1)
    alert_login_fiend = driver.find_element_by_xpath("//*[@id='login-form']/div[1]/p")
    assert alert_login_fiend.text == 'НЕОБХІДНО ЗАПОВНИТИ "ЛОГІН".'
    alert_password_field = driver.find_element_by_xpath("//*[@id='login-form']/div[2]/p")
    assert alert_password_field.text == 'НЕОБХІДНО ЗАПОВНИТИ "ПАРОЛЬ".'
    time.sleep(1)
    print('stop test method invocation1')


# ввести корректный логин, не вводить пароль
def test_fixture_pytest2(driver: WebDriver):
    print('start test method invocation2')
    driver.get('https://rbm-test.brdo.com.ua/login')
    driver.find_element_by_id("loginform-username").send_keys("ndividualkim17+9@gmail.com")
    time.sleep(1)
    driver.find_element_by_name("login-button").click()
    time.sleep(1)
    alert_password_field = driver.find_element_by_xpath("//*[@id='login-form']/div[2]/p")
    assert alert_password_field.text == 'НЕОБХІДНО ЗАПОВНИТИ "ПАРОЛЬ".'
    time.sleep(1)
    print('stop test method invocation2')


# ввести корректный логин, ввести некорректный пароль
def test_fixture_pytest3(driver: WebDriver):
    print('start test method invocation3')
    driver.get('https://rbm-test.brdo.com.ua/login')
    driver.find_element_by_id("loginform-username").send_keys("ndividualkim17+9@gmail.com")
    time.sleep(1)
    driver.find_element_by_id("loginform-password").send_keys("1234567")
    driver.find_element_by_name("login-button").click()
    time.sleep(1)
    alert_password_field = driver.find_element_by_xpath("//*[@id='login-form']/div[2]/p")
    assert alert_password_field.text == 'НЕВІРНИЙ ЛОГІН АБО ПАРОЛЬ'
    time.sleep(1)
    print('stop test method invocation3')


# ввести некорректный логин, ввести корректный пароль
def test_fixture_pytest4(driver: WebDriver):
    print('start test method invocation4')
    driver.get('https://rbm-test.brdo.com.ua/login')
    driver.find_element_by_id("loginform-username").send_keys("ndividualkim17+9@gmail.co")
    time.sleep(1)
    driver.find_element_by_id("loginform-password").send_keys("123456")
    driver.find_element_by_name("login-button").click()
    time.sleep(1)
    alert_password_field = driver.find_element_by_xpath("//*[@id='login-form']/div[2]/p")
    assert alert_password_field.text == 'НЕВІРНИЙ ЛОГІН АБО ПАРОЛЬ'
    time.sleep(1)
    print('stop test method invocation4')


# ввести некорректный логин, ввести некорректный пароль
def test_fixture_pytest5(driver: WebDriver):
    print('start test method invocation5')
    driver.get('https://rbm-test.brdo.com.ua/login')
    driver.find_element_by_id("loginform-username").send_keys("ndividualkim17+9@gmail.co")
    time.sleep(1)
    driver.find_element_by_id("loginform-password").send_keys("12345")
    driver.find_element_by_name("login-button").click()
    time.sleep(1)
    alert_password_field = driver.find_element_by_xpath("//*[@id='login-form']/div[2]/p")
    assert alert_password_field.text == 'НЕВІРНИЙ ЛОГІН АБО ПАРОЛЬ'
    time.sleep(1)
    print('stop test method invocation5')


# не вводить логин, ввести корректный пароль
def test_fixture_pytest6(driver: WebDriver):
    print('start test method invocation6')
    driver.get('https://rbm-test.brdo.com.ua/login')
    time.sleep(1)
    driver.find_element_by_id("loginform-password").send_keys("123456")
    driver.find_element_by_name("login-button").click()
    time.sleep(1)
    alert_login_fiend = driver.find_element_by_xpath("//*[@id='login-form']/div[1]/p")
    assert alert_login_fiend.text == 'НЕОБХІДНО ЗАПОВНИТИ "ЛОГІН".'
    time.sleep(1)
    print('stop test method invocation6')


# не вводить логин, ввести некорректный пароль
def test_fixture_pytest7(driver: WebDriver):
    print('start test method invocation7')
    driver.get('https://rbm-test.brdo.com.ua/login')
    time.sleep(1)
    driver.find_element_by_id("loginform-password").send_keys("1")
    driver.find_element_by_name("login-button").click()
    time.sleep(1)
    alert_login_fiend = driver.find_element_by_xpath("//*[@id='login-form']/div[1]/p")
    assert alert_login_fiend.text == 'НЕОБХІДНО ЗАПОВНИТИ "ЛОГІН".'
    time.sleep(1)
    print('stop test method invocation7')